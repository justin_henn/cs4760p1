//loglib file
//This file is the loglib program for creating astoring logs
//Justin Henn
//2/5/17
//Assignment 1
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include "log.h"

typedef struct list_struct { //This is the list
    data_t item;
    struct list_struct *next;
} log_t;


static log_t *headptr = NULL; 
static log_t *tailptr = NULL;

int addmsg(data_t data){ //Adds the new message to the list

   log_t *newnode;
   int nodesize;
 
   nodesize = sizeof(log_t) + strlen(data.string) + 1;
   if ((newnode = (log_t *)(malloc(nodesize))) == NULL){
     perror("Malloc Failed"); 
     return -1;
   }
   newnode->item.time = data.time;
   newnode->item.nsec = data.nsec;
   newnode->item.string = (char *)newnode + sizeof(log_t);
   strncpy(newnode->item.string, data.string, strlen(data.string));
   newnode->next = NULL;
   if (headptr == NULL)
      headptr = newnode;
   else 
      tailptr->next = newnode;
   tailptr = newnode;
   return 0;

}

void clearlog (void) { //clears out the log to free memory

  log_t *temp = headptr;
  while (temp != NULL) {
    
    temp = headptr->next;
    free(headptr);
    headptr = temp;
  }
}


int savelog (char* arg, const char* filename) { //saves the log list to a file

  FILE *logfile;
  if ((logfile = fopen(filename, "w")) == NULL){ //check to see if it opened a file

   perror("File open error");
   return -1;
  }

  log_t *write_node = headptr;
  fprintf(logfile, "**** Begin Log ***\n");
  while (write_node) { //writes the list node to the file until there is no more
    
    char time[11];
    char nsecs[11];
    sprintf(time, "%lu", (long int)write_node->item.time);
    sprintf(nsecs, "%lu",  write_node->item.nsec);
    fprintf(logfile, "%s: %s%s: Error: nValue = %s\n", arg, time, nsecs,  write_node->item.string);
    write_node = write_node->next;
  }

  fclose(logfile); //close file
  return 0;
}
 

