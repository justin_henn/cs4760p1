//main
//THe is the main file for the loglib program
//Justin Henn
//2/5/17
//Assignment 1
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "log.h"
#include <time.h>

char* makestring(char* str1, char* str2) { //This function connects the nValue with the actual error so the log error is formatted correctly
 
 char* together;
 if ((together = malloc(2 + strlen(str1) + strlen(str2))) == NULL) {
      perror("Malloc failed");
     exit(-1);
  }
 strcpy(together, str1);
 strcat((strcat(together, " ")), str2);
 return together;

}
int sendmsg(char* msg) { //This function creates a data_t to put the log data together then calls the addmsg function to add the log to the list

  struct timespec tps;
  if (clock_gettime(CLOCK_REALTIME, &tps) != 0) {
   perror("Error in getting time");
    return -1;
  }
  data_t data;
  data.time = tps.tv_sec;  
  data.nsec = tps.tv_nsec;
  data.string = msg;

  addmsg(data);

  free(msg);

}

int main (int argc, char **argv) {

  char* argument = argv[0];
  int opt;
  char* filename = NULL;
  char* nValue = NULL;
  extern char* optarg;

  while ((opt = getopt(argc, argv, "hn:l:")) != -1) { //this loop looks at the the arguments and processes them appropriately

    switch (opt) {
    case 'h':
      printf("h - help\nn - specify a value after n for the error code \nl - specify a file name after l for the logfile \n");
      return 0;
    case 'n':
      nValue  = optarg;
      break;
    case 'l':
      
      filename = optarg;
      break;
    }
  }
  
  if (filename == NULL) { //if no filename was givien as an argument
   
     filename = "logfile.txt"; 
  } 

  if (nValue == NULL) { //if no nValue was given as argument

    nValue = "42";  
  }
  
  //test messages 

  sendmsg(makestring(nValue, "This is an error"));
  sendmsg(makestring(nValue, "Program stopped working"));
  sendmsg(makestring(nValue, "Divide by zero"));

 savelog(argument,filename); //saves logs

  clearlog(); //clears the list

  return 0;



}
