CC	= gcc		# The C compiler
CFLAGS	= -g		# Enable debugging by default
TARGET	= loglib
OBJS	= main.o loglib.o 

$(TARGET): $(OBJS)
	$(CC) -o $(TARGET) $(OBJS)

main.o:	main.c log.h
	$(CC) $(CFLAGS) -c main.c

loglib.o: loglib.c log.h
	$(CC) $(CFLAGS) -c loglib.c

clean:
	/bin/rm -f *.o *~ $(TARGET)
