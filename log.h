//log header file
//This file is the header file for the loblib program
//Justin Henn
//2/5/17
//Assignment 1
#include <time.h>

typedef struct data_struct {
    time_t time;
    long nsec;
    char *string;
} data_t;

int addmsg(data_t data);
void clearlog(void);
char *getlog(void);
int savelog(char*, const char *filename);

